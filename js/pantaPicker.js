$(document).ready(function() {
  /* */
    
});

$.fn.pantaPicker = function() {
    
    if( !(this.is('select')) )
        return console.error("pantaPicker Exception: Not a Select Element");
    
    
    _fn_Init(this);
}

function _fn_Init(domElement) {
    
    
    domElement.css("display", "none");
    
    
    var pantaPickerInstance = $('<button id="pantaPickerButton" class="pantaPicker_General">');
    pantaPickerInstance.append($('<button id="'+domElement.attr('id')+'" value=""></button>'))
    
    domElement.attr("id", domElement.attr('id')+"_Old");
    
    $.each(domElement.find('option'), function() {
        
        pantaPickerInstance.append($('<a>'+$(this).val()+'</a>'));

    });
    
    pantaPickerInstance.insertAfter(domElement);
    
    pantaPickerInstance.on('click', 'a', function() {
           
        $(this).parent().find('button').text($(this).text());
        $(this).parent().find('button').val($(this).text());
        $(this).parent().blur();

    });
}